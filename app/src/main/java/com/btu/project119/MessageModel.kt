package com.btu.project119

class MessageModel(
    var message:String? = null,
    var user_id:String? = null,
    var display_name:String? = null,
    var time_stamp:Long? = null
)