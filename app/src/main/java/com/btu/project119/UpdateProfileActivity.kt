package com.btu.project119

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_update_profile.*

class UpdateProfileActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_update_profile)

        auth = FirebaseAuth.getInstance()
        val user = auth.currentUser
        val database = Firebase.database

        confirmUpdateProfileButton.setOnClickListener {
            val myRef = database.getReference("users")
            val studentRef = myRef.child(user!!.uid)
            studentRef.child("name").setValue(nameEditText.text.toString())
            studentRef.child("surname").setValue(surnameEditText.text.toString())
            studentRef.child("age").setValue(ageEditText.text.toString().toInt())
            val addressRef = studentRef.child("address")
            addressRef.child("zip_code").setValue(zipCodeEditText.text.toString())
            addressRef.child("street_name").setValue(streetNameEditText.text.toString())
            addressRef.child("location").setValue(locationEditText.text.toString()).addOnCompleteListener {
                finish()
            }

        }
    }
}