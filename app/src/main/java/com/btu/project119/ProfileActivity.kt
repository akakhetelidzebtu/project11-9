package com.btu.project119

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ServerValue
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.database.ktx.getValue
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_profile.*
import kotlinx.android.synthetic.main.activity_update_profile.*

class ProfileActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        auth = FirebaseAuth.getInstance()

        val user = auth.currentUser

        val database = Firebase.database

        val myRef = database.getReference("chat")
        val listener = object : ValueEventListener{
            override fun onDataChange(snapshot: DataSnapshot) {
                val snapshots = snapshot.children
                var text = ""
                for(item in snapshots){
                    val message = item.getValue<MessageModel>()!!
                    text += "\n" + message.display_name + ": " + message.message
                }

                displayTextView.text = text
            }

            override fun onCancelled(error: DatabaseError) {
            }

        }
        myRef.addValueEventListener(listener)

        updateProfileButton.setOnClickListener {
            val intent = Intent(this,UpdateProfileActivity::class.java)
            startActivity(intent)
        }

        sendButton.setOnClickListener {
            val myRef = database.getReference("chat")
            val message = myRef.push()
            message.child("message").setValue(chatMessageEditText.text.toString())
            message.child("user_id").setValue(user!!.uid)
            message.child("display_name").setValue(user.displayName)
            message.child("time_stamp").setValue(ServerValue.TIMESTAMP)
        }

        logoutButton.setOnClickListener {
            auth.signOut()
            val intent = Intent(this,MainActivity::class.java)

            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION or Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
        }
    }
}